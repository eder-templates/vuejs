// import mdi from '@/plugins/Mdi';
import { createApp } from 'vue';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import { store, key } from './store';

const app = createApp(App);

app.use(router);
app.use(store, key);
app.use(vuetify);
console.log(window.location.origin);
app.mount('#app');
