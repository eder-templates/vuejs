import { InjectionKey } from 'vue';
import { createStore, useStore as baseUseStore, Store } from 'vuex';

// define your typings for the store state
export interface State {
  user?: Record<string, any>;
  token?: Record<string, string>;
  // formState: Record<string, string>;
  // opText: string;
}

export const key: InjectionKey<Store<State>> = Symbol('vuex key');

export const store = createStore<State>({
  state: {
    user: undefined,
    token: undefined,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setToken(state, token) {
      state.token = token;
    },
  },
  getters: {

  },
  actions: {
  },
  modules: {
  },
});

export function useStore() {
  return baseUseStore(key);
}
