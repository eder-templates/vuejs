const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  chainWebpack: config => {
    // set favicon
    config.plugin('html').tap(args => {
      args[0].favicon = './src/favicon.ico';
      return args;
    });
  },
  devServer: {
    proxy: {
      "^/api": {
        target: "https://api.getcolor.localhost/",
        changeOrigin: true,
        logLevel: "debug",
        // pathRewrite: { "^/api": "/" }
      }
    }
  }
});
