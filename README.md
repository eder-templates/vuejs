# VueJS Template

## Setup
- Install and use node version `v18.15.0`
```
nvm install v18.15.0
nvm use v18.15.0
```

- Install dependencies
```
yarn install
```

## Development Server
```
yarn serve
```
